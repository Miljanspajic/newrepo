﻿using ShopServices.Common;
using ShopServices.DataAccess;
using ShopServices.DataAccess.Base;

namespace ShopServices.BusinessLogic  
{
    internal class Supplier 
    {
        private readonly BaseSupplierIntegration _integration;

        public int SupplierId { get; }    

        public Supplier(int supplierId)
        {
             SupplierId = supplierId;
            _integration = DataAccessFactory.GetSupplierIntegration(SupplierId);
        }

        /// <summary>
        /// Evaluates if supplier has desired article in stock
        /// </summary>
        /// <param name="articleId">Identification number of desired article</param>
        /// <returns>True if supplier has article otherwise false</returns>
        public bool ArticleInInventory(int articleId) => _integration.ArticleInInventory(articleId);

        /// <summary>
        /// Get's article from supplier stock
        /// </summary>
        /// <param name="articleId">Identification number of desired article</param>
        /// <remarks> Shouldn't be called if ArticleInInvetory didn't returned true before
        /// </remarks>
        /// <returns>Article</returns>
        public Article GetArticle(int articleId) => _integration.GetArticle(articleId);
    }
}
