﻿using ShopServices.Common;
using ShopServices.DataAccess;
using ShopServices.Properties;
using System;
using System.Collections.Generic;


namespace ShopServices.BusinessLogic  
{
    internal class DatabaseDriver
    {
        /// <summary>
        /// Instantiates the right database manager and try's to save article
        /// </summary>
        /// <param name="article">Article that should be saved</param>
        public static void Save(Article article)
        {
            try
            {
                var dao = DataAccessFactory.GetShopDAO();
                dao.SaveArticle(article);
            }
            catch(Exception ex)
            {
                Logger.Instance.WriteLog(Resources.ErrorDataAccess + ex);
                throw;
            }
        }

        /// <summary>
        /// Instantiates the right database manager and try's to get available suppliers
        /// </summary>
        /// <returns>List of available suppliers</returns>
        public static List<Supplier> GetSuppliers()
        {
            List<Supplier> result = null;
            try
            {
                var dao = DataAccessFactory.GetShopDAO();
                result = dao.GetSuppliers();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Resources.ErrorDataAccess + ex);
            }
            return result;
        }

        /// <summary>
        /// Instantiates the right database manager and try's to get desired article
        /// </summary>
        /// <param name="articleId">Identification number of desired article</param>
        /// <returns>Article if found otherwise null</returns>
        public static Article GetArticleById(int articleId)
        {
            Article article = null;
            try
            {
                var dao = DataAccessFactory.GetShopDAO();
                article = dao.GetArticleById(articleId);
            }
            catch(Exception ex)
            {
                Logger.Instance.WriteLog(Resources.ErrorDataAccess + ex);
            }
            return article;
        }
    }
}
