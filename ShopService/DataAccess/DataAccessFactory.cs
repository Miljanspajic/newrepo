﻿using ShopServices.DataAccess.Base;
using ShopServices.DataAccess.Mock;

namespace ShopServices.DataAccess
{
    internal class DataAccessFactory
    {
        /// <summary>
        /// Instantiates proper Shop dataaccess manager runtime
        /// </summary>
        /// <returns>Proper shop dataaccess manager</returns>
        public static BaseShopDAO GetShopDAO()
        {
            //here should be logic that evaluates which ShopDAO should be used
            return new MockShopDAO();
        }

        /// <summary>
        /// Instantiates proper supplier dataaccess manager runtime
        /// </summary>
        /// <returns>Proper supplier dataaccess manager</returns>
        public static BaseSupplierIntegration GetSupplierIntegration(int supplierId)
        {
            //here should be logic that evaluates which SupplierIntegration should be used
            return new MockSupplierIntegration(supplierId);
        }

    }
}
