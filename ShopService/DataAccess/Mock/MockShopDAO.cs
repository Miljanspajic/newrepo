﻿using ShopServices.BusinessLogic;
using ShopServices.Common;
using ShopServices.DataAccess.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using ShopServices.Properties;

namespace ShopServices.DataAccess.Mock
{
    internal class MockShopDAO : BaseShopDAO
    {
        private static readonly string _sourceFile= System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/" + "DataAccess" + "/" + "Mock" + "/" + "XML" + "/" + "Suppliers.xml");

        private static readonly  List<Article> _articles=new List<Article>();

        #region public overriden methods

        /// <summary>
        /// Mock implementation of BaseShopDAO's SaveArticle method
        /// </summary>
        /// <remarks>Adds article to class's list of articles
        /// </remarks>
        /// <param name="article">Article that should be added</param>
        public override void SaveArticle(Article article)
        {
            _articles.Add(article);
        }

        /// <summary>
        /// Mock implementation of BaseShopDAO's GetSuppliers method
        /// </summary>
        /// <remarks>Returns suppliers found in Suppliers.xml file
        /// </remarks>
        /// <returns>List of found suppliers otherwise null</returns>
        public override List<Supplier> GetSuppliers()
        {
            List<Supplier> result = null;    

            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(_sourceFile);

                if (!fileInfo.Exists)
                    throw new Exception("File not found: " + _sourceFile);

                System.Xml.XmlDocument xdoc = new System.Xml.XmlDocument();

                xdoc.Load(_sourceFile);

                var suppliers = xdoc.SelectNodes("//Supplier");

                if (suppliers != null && suppliers.Count > 0)
                {
                    result = new List<Supplier>();
                    for (int i = 0; i < suppliers.Count; i++)
                    {
                        var supplierId = int.Parse(suppliers[i].SelectSingleNode("./SupplierId").InnerText);
                        var supplier = new Supplier(supplierId);
                        result.Add(supplier);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Resources.ErrorMockSuppliers + ex);
            }
            return result;
        }

        /// <summary>
        /// Mock implementation of BaseShopDAO's GetArticleById method
        /// </summary>
        /// <remarks>Returns article if article with same identification number exists in class's articles list
        /// </remarks>
        /// <param name="articleId">Article identification number</param>
        /// <returns>Article if found otherwise null</returns>
        public override Article GetArticleById(int articleId)
        {
            Article result = null;
            if (_articles != null && _articles.Any())
            {
                result = _articles.FirstOrDefault(item => item.ID == articleId);
            }
            return result;
        }

        #endregion
    }
}
