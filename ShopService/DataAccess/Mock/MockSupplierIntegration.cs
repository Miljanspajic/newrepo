﻿using ShopServices.Common;
using ShopServices.DataAccess.Base;
using System;
using System.Linq;
using System.Xml;
using ShopServices.Properties;
namespace ShopServices.DataAccess.Mock
{
    internal class MockSupplierIntegration : BaseSupplierIntegration
    {

        private static readonly string _sourceFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/" + "DataAccess" + "/" + "Mock" + "/" + "XML" + "/" + "Suppliers.xml");

        private readonly int _supplierId;

        public MockSupplierIntegration(int supplierId)
        {
            _supplierId = supplierId;
        }

        #region public overriden methods

        /// <summary>
        /// Mock implementation of BaseSupplierIntegration's GetArticle method
        /// </summary>
        /// <remarks>Returns article if found in Suppliers.xml,article with this articleid must exist
        /// in inventory of supplier with this class supplierid 
        /// </remarks>
        /// <param name="articleId">Article identification number that should be returned</param>
        /// <returns>List of found suppliers otherwise null</returns>
        public override Article GetArticle(int articleId)
        {
            Article result = null;

            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(_sourceFile);
                if (!fileInfo.Exists)
                    throw new Exception("File not found: " + _sourceFile);

                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(_sourceFile);

                //find inventory of this supplier
                var inventory = (from XmlNode x in xdoc.SelectNodes("//Supplier")
                                 where int.Parse(x.SelectSingleNode("./SupplierId").InnerText) == _supplierId
                                 select x.SelectSingleNode("./Inventory")).FirstOrDefault().Clone();

                if (inventory != null)
                {
                    //see if article with this id exist in suppliers inventory
                    var article = (from XmlNode x in inventory.SelectNodes("//Article")
                                   where int.Parse(x.SelectSingleNode("./ID").InnerText) == articleId
                                   select x).FirstOrDefault();

                    if (article != null)
                    {
                        result = new Article
                        {
                            ID = int.Parse(article.SelectSingleNode("./ID").InnerText),
                            NameOfArticle = article.SelectSingleNode("./NameOfArticle").InnerText,
                            ArticlePrice = int.Parse(article.SelectSingleNode("./ArticlePrice").InnerText)
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Resources.ErrorMockArticle + ex);
            }
            return result;
        }

        /// <summary>
        /// Mock implementation of BaseSupplierIntegration's ArticleInInventory method
        /// </summary>
        /// <remarks>Returns true if found in Suppliers.xml,article with this articleid must exist
        /// in inventory of supplier with this class supplierid 
        /// </remarks>
        /// <param name="articleId">Article identification number that should be found</param>
        /// <returns>True if found otherwise false</returns>
        public override bool ArticleInInventory(int articleId)
        {
          
            try
            {
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(_sourceFile);
                if (!fileInfo.Exists)
                    throw new Exception("File not found: " + _sourceFile);

                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(_sourceFile);

                //find inventory of this supplier
                var inventory = (from XmlNode x in xdoc.SelectNodes("//Supplier")
                                 where int.Parse(x.SelectSingleNode("./SupplierId").InnerText) == _supplierId
                                 select x.SelectSingleNode("./Inventory")).FirstOrDefault().Clone();

                if (inventory != null)
                {
                    //see if article with this id exist in suppliers inventory
                    var article = (from XmlNode x in inventory.SelectNodes("//Article")
                                   where int.Parse(x.SelectSingleNode("./ID").InnerText) == articleId
                                   select x).FirstOrDefault();

                    if (article != null)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Resources.ErrorMockInventory + ex);
            }
            return false;
        }

        #endregion

    }
}
