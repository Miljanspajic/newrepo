﻿using ShopServices.Common;

namespace ShopServices.DataAccess.Base
{
    internal abstract class BaseSupplierIntegration
    {
        public abstract Article GetArticle(int articleId);

        public abstract bool ArticleInInventory(int articleId);
    }
}
