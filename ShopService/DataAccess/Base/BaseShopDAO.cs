﻿using ShopServices.BusinessLogic;
using ShopServices.Common;
using System.Collections.Generic;

namespace ShopServices.DataAccess.Base
{
    internal abstract class BaseShopDAO
    {
        /// <summary>
        /// Saves article in shop database
        /// </summary>
        /// <param name="article">Article that shold be saved</param>
        public abstract void SaveArticle(Article article);

        /// <summary>
        /// Get's suppliers from shop database
        /// </summary>
        /// <returns>List of available suppliers</returns>
        public abstract List<Supplier> GetSuppliers();

        /// <summary>
        /// Goes to the shop database and returns article
        /// </summary>
        /// <param name="articleId">Article identification number</param>
        /// <returns>Article</returns>
        public abstract Article GetArticleById(int articleId);
    }
}
