﻿using ShopServices.BusinessLogic;
using ShopServices.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using ShopServices.Properties;

namespace ShopServices
{
    public class ShopService
    {
        private static ShopService _instance;

        private List<Supplier> _suppliers;

        public static ShopService Instance => _instance ?? (_instance = new ShopService());


        private ShopService() => GetSuppliers();

        #region public client visible methods

        /// <summary>
        /// Finds if the desired article is available and sells it 
        /// </summary>
        /// <param name="articleId">Identification number of the desired article</param>
        /// <param name="maxExpectedPrice">Maximal price buyer is willing to give for selected article</param>
        /// <param name="buyerId">Buyer's identification number</param>
        /// <param name="errorCode">error message</param>
        ///<permission cref="System.Security.PermissionSet">Client 
        ///can access this method.</permission>
        public void OrderAndSellArticle(int articleId, int maxExpectedPrice, int buyerId,out string errorCode)
        {
            errorCode = string.Empty;

            if (_suppliers == null)
            {
                Logger.Instance.Error(Resources.NoSupplier);
                throw new Exception(Resources.NoSupplier);
            }

            Logger.Instance.Debug("Trying to order article " + articleId + "  MaxExpectedPrice:" + maxExpectedPrice);

            Article article = OrderArticle(articleId, maxExpectedPrice);

            if (article == null)
            {
                Logger.Instance.Info(Resources.NoArticleFound);
                errorCode = Resources.NoArticleFound;
                return;
            }

            Logger.Instance.Info("Article ordered");

            Logger.Instance.Debug("Trying to sell article with id=" + articleId);

            if (!SellArticle(article, buyerId))
            {
                errorCode = Resources.ErrorSavingArticle;
                return;
            }
        }

        /// <summary>
        /// Finds if the desired article is sold and in the inventory
        /// </summary>
        /// <param name="articleId">Identification number of the article</param>
        /// <param name="errorCode">error message</param>
        ///<permission cref="System.Security.PermissionSet">Client 
        ///can access this method.</permission>
        /// <returns>Article if found else System.Exception</returns>
        /// <exception cref="System.Exception">Thrown when article
        /// hasn't be found in shop's inventory.</exception>
        public Article GetById(int articleId,out string errorCode)
        {
            errorCode = string.Empty;

            Article article = DatabaseDriver.GetArticleById(articleId);

            if (article == null)
            {
                Logger.Instance.Info(Resources.NoArticleFound);
                errorCode = Resources.NoArticleFound;
            }

            return article;
        }

        #endregion

        #region private methods

        /// <summary>
        /// Fills the list of the available suppliers
        /// </summary>
        private void GetSuppliers() => _suppliers = DatabaseDriver.GetSuppliers();

        /// <summary>
        /// Loops through the list of suppliers and try's to find the one that satisfies maximal expected price condition
        /// </summary>
        /// <param name="articleId">Article identification number</param>
        /// <param name="maxExpectedPrice">Maximal expected price buyer is willing to give</param>
        /// <returns>Article if found else null</returns>
        private Article OrderArticle(int articleId, int maxExpectedPrice)
        {
            Article article = null;

            var suppliersWithArticle = (from supplier in _suppliers
                where supplier.ArticleInInventory(articleId)
                select supplier);

            if (suppliersWithArticle != null && suppliersWithArticle.Any())
            {
                Logger.Instance.Debug("Supplier with this article found");

                article = (from x in suppliersWithArticle
                    let y = x.GetArticle(articleId)
                    where y != null && y.ArticlePrice <= maxExpectedPrice
                    select y).FirstOrDefault();
            }

            return article;
        }

        /// <summary>
        /// Trys to sell desired article
        /// </summary>
        /// <param name="article">Article that should be sold</param>
        /// <param name="buyerId">Buyer identification number</param>
        /// <returns>True if succesfull otherwise false</returns>
        private bool SellArticle(Article article, int buyerId)
        {
            article.IsSold = true;
            article.BuyerUserId = buyerId;
            article.SoldDate = DateTime.Now;

            try
            {
                DatabaseDriver.Save(article);
                Logger.Instance.Info("Article with id=" + article.ID + " is sold.");
            }
            catch (Exception ex)
            {
                Logger.Instance.Error(Resources.ErrorSavingArticle + "=" + article.ID + "/n" + ex);
                return false;
            }

            return true;
        }

        #endregion
    }
}
