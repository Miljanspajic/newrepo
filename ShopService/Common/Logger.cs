﻿using System;
using System.IO;
using System.Text;

namespace ShopServices.Common  
{
    public class Logger
    {
        private static Logger _instance;

        private readonly FileStream _fileStream;

        private readonly StreamWriter _streamWriter;

        private readonly string _sourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "/" + "Logs");

        private bool _disposed ;

        public static Logger Instance => _instance ?? (_instance = new Logger());

        private Logger()
        {
            string fileName = Path.Combine(_sourceFile + "/" + DateTime.Today.ToString(@"yyyy-MM-dd") + ".log");

            if (!Directory.Exists(_sourceFile))
                Directory.CreateDirectory(_sourceFile);

            _fileStream = File.OpenWrite(fileName);
            _streamWriter = new StreamWriter(_fileStream);
        }

        /// <summary>
        /// Writes message to the log file
        /// </summary>
        /// <param name="message">messege that should be written in log file</param>
        public void WriteLog(string message)
        {

            StringBuilder formattedMessage = new StringBuilder();
            formattedMessage.AppendLine("Date: " + DateTime.Now.ToString(@"yyyy-MM-dd h:mm tt"));
            formattedMessage.AppendLine("Message: " + message);
            _streamWriter.WriteLine(formattedMessage.ToString());
            _streamWriter.Flush();
        }

        public void Info(string message)
        {
            Console.WriteLine("Info: " + message);
        }

        public void Error(string message)
        {
            WriteLog(message);
            Console.WriteLine("Error: " + message);
        }

        public void Debug(string message)
        {
#if DEBUG
            Console.WriteLine("Debug: " + message);
#endif
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _streamWriter?.Dispose();
                }

                _disposed = true;
            }

        }
    }
}
