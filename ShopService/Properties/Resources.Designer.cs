﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShopServices.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ShopServices.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Errer retrieving data from data access!.
        /// </summary>
        internal static string ErrorDataAccess {
            get {
                return ResourceManager.GetString("ErrorDataAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while getting suppliers!.
        /// </summary>
        internal static string ErrorGettingSuppliers {
            get {
                return ResourceManager.GetString("ErrorGettingSuppliers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while geting Mock article.
        /// </summary>
        internal static string ErrorMockArticle {
            get {
                return ResourceManager.GetString("ErrorMockArticle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while getting mock inventory.
        /// </summary>
        internal static string ErrorMockInventory {
            get {
                return ResourceManager.GetString("ErrorMockInventory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error while getting Mock suppliers.
        /// </summary>
        internal static string ErrorMockSuppliers {
            get {
                return ResourceManager.GetString("ErrorMockSuppliers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error Saving Article.
        /// </summary>
        internal static string ErrorSavingArticle {
            get {
                return ResourceManager.GetString("ErrorSavingArticle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Could not order article.
        /// </summary>
        internal static string NoArticleFound {
            get {
                return ResourceManager.GetString("NoArticleFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No supplier found!.
        /// </summary>
        internal static string NoSupplier {
            get {
                return ResourceManager.GetString("NoSupplier", resourceCulture);
            }
        }
    }
}
