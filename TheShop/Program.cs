﻿using ShopServices;
using System;
using System.Runtime.CompilerServices;

namespace TheShop
{
	internal class Program
	{
		private static void Main(string[] args)
		{		
		    string input = string.Empty;
		    do
		    {
		        Console.WriteLine("If you want to continue press any key + enter otherwise type exit + enter");

                input = Console.ReadLine();

		        if (!input.Equals("exit", StringComparison.OrdinalIgnoreCase))
		        {
		            DoShopping();
		        }
		    } while (!input.Equals("exit", StringComparison.OrdinalIgnoreCase));		
		}

	    private static void DoShopping()
	    {
	        var shopService = ShopService.Instance;

            Console.WriteLine("ArticleID :" );

	        var articleId = GetInputFromConsole();

            Console.WriteLine("Maximal Expected Price :");

	        var maxExpectedPrice = GetInputFromConsole();

	        Console.WriteLine("BuyerID:");

	        var buyerId = GetInputFromConsole();

	        string errorCode = string.Empty;

            try
	        {
	            //order and sell
	            shopService.OrderAndSellArticle(articleId, maxExpectedPrice, buyerId,out errorCode);
	        }
	        catch (Exception ex)
	        {
	            Console.WriteLine(ex);
	            return;
	        }
	        if (!string.IsNullOrEmpty(errorCode))
	        {
                Console.WriteLine(errorCode);
	            return;
	        }
	        try
	        {
	            //print article on console
	            var article = shopService.GetById(articleId,out errorCode);
	        }
	        catch (Exception ex)
	        {
	            Console.WriteLine(ex);
	            return;
	        }
	        if (!string.IsNullOrEmpty(errorCode))
	        {
                Console.WriteLine(errorCode);
	            return;
	        }
	        Console.WriteLine("Article with ID : " + articleId + "found");
	    }

	    private static int GetInputFromConsole()
	    {
	        try
	        {
	            var number = int.Parse(Console.ReadLine());
	            return number;
	        }
	        catch
	        {
                Console.WriteLine("Please enter valid number");
	            return GetInputFromConsole();
	        }
	    }
	}
}