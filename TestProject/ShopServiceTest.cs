﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopServices;

namespace TestProject
{
    [TestClass]
    public class ShopServiceTest
    {
        [TestMethod]
        public void OrderAndSellArticle()
        {
            var shopService = ShopService.Instance;
            int articleId = 1;
            int maxExpectedPrice = 1000;
            int buyeriD = 10;
            string errorCode = string.Empty;
            shopService.OrderAndSellArticle(articleId, maxExpectedPrice, buyeriD,out errorCode);
        }     
    }
}
